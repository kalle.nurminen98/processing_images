#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <filesystem>

using json = nlohmann::json;

int main(int, char**)
{
    // Creating original folder
    std::string folder_name = "original";
    std::string creating_original_file = "mkdir " + folder_name;
    system(creating_original_file.c_str());

    // Creating output folder
    std::string folder_name_output = "output";
    std::string creating_output_file = "mkdir " + folder_name_output;
    system(creating_output_file.c_str());

    // Getting connection to server
    RestClient::Response r = RestClient::get("https://aws.random.cat/meow");
    std::cout << r.body << std::endl;
    // Parse
    json json_body = json::parse(r.body);
    // Folder paths
    std::string folder_path_to_output = "~/projects/processing_images/output";
    std::string folder_path_to_original = "~/projects/processing_images/original";
    // cat_img to string
    std::string cat_img = json_body["file"].dump();
    std::cout << cat_img << std::endl;

    // Filesystem paths
    std::filesystem::path path {folder_name.c_str()};
    std::filesystem::path path_output {folder_name_output.c_str()};

    int counter = 0;
    // Getting sum of pictures in original folder
    for (auto& i : std::filesystem::directory_iterator(path))
    {
        ++counter;
    }
    // Putting picture from server to original folder
    std::string wget = "wget -P " + folder_path_to_original + " " + cat_img;
    system(wget.c_str());
    // Automated process for picture naming
    std::string new_name_for_image = std::to_string(counter) + "_cat.jpg";
    std::size_t after_last_slash = cat_img.find_last_of("/");
    std::string cat_img_jpg = cat_img.substr(after_last_slash+1);
    // Removing quotes
    cat_img_jpg.erase(remove(cat_img_jpg.begin(),cat_img_jpg.end(), '\"'),cat_img_jpg.end());
    std::cout << cat_img_jpg << std::endl;
    // Renaming picture to original folder
    std::filesystem::rename(path / cat_img_jpg, path / new_name_for_image);
    

    std::string window_name = "Cat picture 300x300";
    cv::namedWindow(window_name);
    // Reading recent picture from folder
    cv::Mat picture = cv::imread(path/new_name_for_image);
    cv::Mat picture_resized, picture_border, src_gray, picture_blackandwhite;
    // Resizing picture to right size
    cv::resize(picture, picture_resized, cv::Size(300, 300), cv::INTER_LINEAR);

    int input;
    int top, bottom, left, right;
    int border = cv::BORDER_CONSTANT;
    cv::RNG rng(12345);
    top = (int) (0.05*picture_resized.rows); bottom = top;
    left = (int) (0.05*picture_resized.cols); right = left;

    int threshold_value = 0;
	int threshold_type = 3;
    int const max_binary_value = 255;
    // Switch statement
    std::cout << "Choose a number for image processing" << std::endl;
    std::cout << "1 for border for the image" << std::endl;
    std::cout << "2 for thresholding for the image" << std::endl;
    std::cout << "3 for both image processes for the image" << std::endl;
    std::cout << "Press ESC-keybind to exit from picture" << std::endl;
    std::cin >> input;
    switch(input)
    {
        // Borders for picture
        case 1:
        {
            // Random color
            cv::Scalar color_value(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0,255));
            // Making border to picture
            cv::copyMakeBorder(picture_resized, picture_border, top, bottom, left, right, border, color_value);
            // Showing picture, press ESC-keybind to exit
            while(true)
            {
                cv::imshow(window_name, picture_border);
                // ESC-key
                if(cv::waitKey(10) == 27)
                {
                    break;
                }
            }
            // Making algorithm to renaming picture
            std::string border_picture = "border_" + std::to_string(counter) + "_cat.jpg";
            // renaming picture to output file
            cv::imwrite(path_output / border_picture, picture_border); 
            break;
        }
	    // Black and white
	    case 2:
        {
   	        // Convert the image to Gray
    	    cv::cvtColor(picture_resized, src_gray, cv::COLOR_BGR2GRAY ); // Convert the image to Gray
           //setting treshold
	        cv::threshold( src_gray, picture_blackandwhite, threshold_value, max_binary_value, threshold_type );
    

	    
            // Showing picture, press ESC-keybind to exit
            while(true)
            {
                cv::imshow(window_name, picture_blackandwhite);
                // ESC-key
                if(cv::waitKey(10) == 27)
                {
                    break;
                }
            }
            // Making algorithm to renaming picture
            std::string blckandwhite_picture = "blackandwhite_" + std::to_string(counter) + "_cat.jpg";
            // renaming picture to output file
            cv::imwrite(path_output / blckandwhite_picture, picture_blackandwhite);
            break;
        }
        case 3:
        {
            // Convert the image to Gray
    	    cv::cvtColor(picture_resized, src_gray, cv::COLOR_BGR2GRAY ); // Convert the image to Gray
           //setting treshold
	        cv::threshold( src_gray, picture_blackandwhite, threshold_value, max_binary_value, threshold_type );

            // Random color
            cv::Scalar color_value(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0,255));
            // Making border to picture
            cv::copyMakeBorder(picture_blackandwhite, picture_border, top, bottom, left, right, border, color_value);

            while(true)
            {
                cv::imshow(window_name, picture_border);
                // ESC-key
                if(cv::waitKey(10) == 27)
                {
                    break;
                }
            }
            // Making algorithm to renaming picture
            std::string border_picture = "both_image_processes" + std::to_string(counter) + "_cat.jpg";
            // renaming picture to output file
            cv::imwrite(path_output / border_picture, picture_border); 
            break;
        }
    }

    return 0;
}
