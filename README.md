# Image processor

Idea was to get picture and process it different ways like put borders to it.

There is small restriction, if you get .gif or .jpeg then this program will not work and you need to build it again.

# Usage

You need g++11 compiler for this project.

Take a git clone on project and take it to your computer.

Check your file paths are correct.

For building project:

on main type 

$code .

In visual studio code press f1 key and type cmake and choose build

Use g++11 compiler

After building use command 

$./build/images on terminal

# About the project

This was made by Kalle Nurminen and Ilona Skarp for Noroff and Experis Academy Finland.

# Responsibilities

Kalle: Created folders and got the picture from server and renamed it to original file. Resized picture and created border process in switch statement.

Ilona: Created black and white picture process in switch statement. Helping KAlle with ideas on automation of files.

# Maintainers

[Ilona Skarp] (https://gitlab.com/iskarp)

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)
